import React, { useState, useContext } from 'react'
import './Signin-up.css'
import { Link } from 'react-router-dom'
import axios from 'axios'
import CircularProgress from '@material-ui/core/CircularProgress';
import { AuthContext } from '../Context/AuthContext'
import md5 from "md5"
function Signin() {

    const [name, setName] = useState()   /* Instead of Using useState hook we can use useRef and keep ref in the required input field for getting data */
    const [password, setPassword] = useState()
    const [error,setError] = useState("")
    const { isFetching, dispatch } = useContext(AuthContext)

    const API_URL = process.env.REACT_APP_API_URL

    const loginCall = async (userCredential, dispatch) => {
        dispatch({ type: "LOGIN_START" });
        try {
            // const res = await axios.post("http://localhost:5000/api/auth/signin", userCredential);
            const response = await axios({
                method: 'get',
                headers : {"Content-type" : "application/json" },
                url: 'http://localhost:3001/whatsapp/',
            })
            const  data = await response
            const allUser = data.data.data
            for(const user  of allUser){
                if(user.userName == name  && user.password == md5(password) ){
            sessionStorage.setItem("current_user" , JSON.stringify({user_id :user._id , userName : user.name}))
            dispatch({ type: "LOGIN_SUCCESS", payload: user });
              }
            }
        }
        catch (err) {
            dispatch({ type: "LOGIN_FAILURE", payload: err })
            setError("Wrong Password Or Username")
        }
    }
        const handleForm = (e) => {
        e.preventDefault()
        loginCall({ name, password }, dispatch)
    }

    return (
        <div className='signin-container'>
            <div className="signin-card">
                <form onSubmit={handleForm}>
                    <h2 className="signin-title"> Log in</h2>
                    <p className="line"></p>
                    <div className="error-message"><p>{error}</p></div>
                    <div className="signin-fields">
                        <label htmlFor="email"> <b>User Name</b></label>
                        <input className='signin-textbox' type="text" placeholder="Enter UserName" name="username" required onChange={(e) => { setName(e.target.value) }} />
                        <label htmlFor="password"><b>Password</b></label>
                        <input className='signin-textbox' type="password" minLength='6' placeholder="Enter Password" name="psw" required onChange={(e) => { setPassword(e.target.value) }} />
                    </div>
                    <button className="signin-button" disabled={isFetching}>{isFetching ? <CircularProgress color="#ffffff" /> : "Log In"}</button>
         
                </form>
              
            </div>
        </div>
    )
}

export default Signin
