
///import 
import express from 'express'
import exampleRouter from './router/route.js'
import mongoose from './database/dataBase.js'
import cors from "cors"
import bodyparser from 'body-parser'
import md5 from "md5"
// import createDoc  from './schema/studentSchema.js'
// createDoc()
     const app = express()
     const port = process.env.PORT || 3001;  
/*  -----Routing----    */
app.use(cors())
app.use('/whatsapp',exampleRouter);

/*  -----Routing End---- */
app.listen(port,()=>{
    console.log('server connected at http://localhost:3001')
})