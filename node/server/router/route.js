//import 
import express from 'express';
import exampleController from '../controller/controller.js'
import bodyparser from 'body-parser'
const router = express.Router();

// Body-parser middleware
router.use(bodyparser.urlencoded({extended:false}))
router.use(bodyparser.json())


router.get('/',exampleController.userinfo)

router.post('/signin',exampleController.finduser)

router.get('/user',exampleController.user)

router.post("/new_Contact", exampleController.new_Contact)

router.post("/all_Contacts" , exampleController.all_Contacts)

export default router