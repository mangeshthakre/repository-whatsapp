//import //
import mongoose from '../database/studentDB.js'
///

const studentSchema = new mongoose.Schema({
    firstName:{type:String, required:true},
    lastName:{type:String, required:true},
         age:{type:Number, required:true, min:18, max:60},
     //    fees:{type:mongoose.decimal128, required:true},
   is_active:{type:Boolean ,default:1 },
  created_at:{type:Date, default:Date.now},
  deleted_at:{type:Date, default:''}
})

const studentModel = mongoose.model( 'student' , studentSchema)

export default studentModel ;


// const createDoc= async()=>{
//     try{
//         const studentDoc =  new studentModel({
//              firstName:'mangesh',
//              lastName:'thakre',
//              age : 21,
//              })     
//    const result = await studentDoc.save()
//    console.log(result)
//    return 'hello'
//     }catch (err){
//         console.log(err)
//     }
//     }
   
