import mongoose from '../database/dataBase.js'


const userSchema = new mongoose.Schema({
    userName:{type:String, required:true},
    password:{type:String, required:true},
    InstanceId : {type: String , required : true} ,
    phoneNo : {type :Number , required : true}
    })

const userModel = mongoose.model( 'user' , userSchema)
export default userModel