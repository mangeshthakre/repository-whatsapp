import mongoose  from "../database/dataBase.js";

const newContact = new mongoose.Schema({
    phoneNo : {type:Number , required : true },
    InstanceId : { type : String , required : true },
    userName : {type :String , required : true },
    userPhoneNo : {type : Number ,required :true } 

})


const newContactModel  = mongoose.model("newContacts" , newContact )
export default newContactModel